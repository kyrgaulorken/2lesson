package com.ork.intentssharedprefs

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    companion object {
         const val USERNAME_KEY = "username"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val myPref = getSharedPreferences("app_prefs", MODE_PRIVATE)

        val user = intent.getParcelableExtra<User>(USERNAME_KEY)
        Log.d("asdasdasd",user.toString())

        //UserNameFromFirst.text = "${user.name} ${user.age}"
        UserNameFromFirst.text = myPref.getString("name","").toString() + " "+
                myPref.getString("age","").toString()

    }
}