package com.ork.intentssharedprefs

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ork.intentssharedprefs.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var mainBinding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainBinding.root)

        mainBinding.nextPage.setOnClickListener {
            val intent = Intent(this,SecondActivity::class.java)
            val text = mainBinding.username.text.toString()
            val age = age.text.toString()

            PrefManager.putName(this,text,"name")
            PrefManager.putName(this,age,"age")

            val user = User(text,age)
            intent.putExtra(SecondActivity.USERNAME_KEY,user)
            startActivity(intent)
        }
    }

}