package com.ork.intentssharedprefs

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity

object PrefManager {
    private fun getPrefs(context: Context): SharedPreferences = context.getSharedPreferences("app_prefs", AppCompatActivity.MODE_PRIVATE)
    fun putName(context : Context, name : String, key : String) = getPrefs(context).edit().putString(key,name).apply()
}